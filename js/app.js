$(document).ready(function() {
  // Recuperamos la plantilla definida en el html.
  const source = $('#template').html();
  //Compilamos la plantilla en una funcion.
  const template = Handlebars.compile(source);
  $('#search').click(function() {
    $('#result-placeholder').empty();
    const query = $('#query').val();
    if (query) {
      //El método GET implementa la interfaz de una promesa, proporcionandole
      //todas las propiedades, métodos y comportamientos de una promesa.
      $.get('https://api.spotify.com/v1/search', {
          q: query,
          type: 'track'
        }).done(function(data) {
          //Pasamos a la plantilla un objeto con los datos que ésta necesita, e
          //insertamos el código html generado en la página.
          $('#result-placeholder').append(template(data));
        }).fail(function(error) {
          console.log(error);
        });
    }
  });
});
